INTRODUCTION
------------
This module allows the addtion of the Widget Bootstrap buttons from CKEditor.com in your WYSIWYG. http://ckeditor.com/addon/widgetbootstrap

INSTALLATION
------------
1. Download the plugin from http://ckeditor.com/addon/widgetbootstrap.
2. Add the downloaded plugin into the libraries directory. (/libraries).
3. Enable the ckeditor_widgetbootstrap module from the 'Extend' screen.
4. Visit `/admin/config/content/formats` and add the buttons to your format.

REQUIREMENTS
------------
CKEditor Module (Core)

KNOWN ISSUES
------------
According to https://github.com/albatrossdigital/widgetbootstrap/issues/1 the alert box doesn't work out of the box.