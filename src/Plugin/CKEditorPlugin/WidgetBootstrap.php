<?php

/**
 * @file
 * Definition of \Drupal\ckeditor_bootstrap_widgets\Plugin\CKEditorPlugin\WidgetBootstrap.
 */

namespace Drupal\ckeditor_bootstrap_widgets\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Widget Bootstrap" plugin.
 *
 * @CKEditorPlugin(
 *   id = "widgetbootstrap",
 *   label = @Translation("Widget Bootstrap")
 * )
 */
class WidgetBootstrap extends CKEditorPluginBase {

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::isInternal().
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getFile().
   */
  public function getFile() {
    return 'libraries/widgetbootstrap/plugin.js';
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
   */
  public function getButtons() {
    $path = '/libraries/widgetbootstrap';
    return [
      'WidgetbootstrapLeftCol' => [
        'label' => t('Widget Bootstrap Left'),
        'image' => $path . '/icons/widgetbootstrapLeftCol.png'
      ],
      'WidgetbootstrapRightCol' => [
        'label' => t('Widget Bootstrap Right'),
        'image' => $path . '/icons/widgetbootstrapRightCol.png'
      ],
      'WidgetbootstrapTwoCol' => [
        'label' => t('Widget Bootstrap Two Col'),
        'image' => $path . '/icons/widgetbootstrapTwoCol.png'
      ],
      'WidgetbootstrapAccordion' => [
        'label' => t('Widget Bootstrap Accordion'),
        'image' => $path . '/icons/widgetbootstrapAccordion.png'
      ],
      'WidgetbootstrapAlert' => [
        'label' => t('Widget Bootstrap Alert'),
        'image' => $path . '/icons/widgetbootstrapAlert.png'
      ],
      'WidgetbootstrapThreeCol' => [
        'label' => t('Widget Bootstrap Three Col'),
        'image' => $path . '/icons/widgetbootstrapThreeCol.png'
      ]
    ];
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getConfig().
   */
  public function getConfig(Editor $editor) {
    return [];
  }


}